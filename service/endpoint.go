package service

import (
	"encoding/base64"
	"fmt"
	"gitlab.com/ghostbutler/tool/service"
	"net"
	"net/http"
	"strings"
)

const (
	APIServiceV1SecurityManagerCreateAPIKey = common.APIServiceType(iota + common.APIServiceBuiltInLast)
	APIServiceV1SecurityManagerCreateOnceAPIKey
	APIServiceV1SecurityManagerGetServiceByAPIKey
)

var SecurityManagerAPIService = map[common.APIServiceType]*common.APIEndpoint{
	APIServiceV1SecurityManagerCreateAPIKey: {
		Path:                    []string{"api", "v1", "securitymanager", "api"},
		Method:                  "POST",
		Description:             "Create a public/private api key set based on basic authentication result",
		Callback:                HandleAPIKeyRequest,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1SecurityManagerCreateOnceAPIKey: {
		Path:                    []string{"api", "v1", "securitymanager", "api", "once"},
		Method:                  "POST",
		Description:             "Create a public api key valid for only one used based on public/private key delivered previously",
		Callback:                HandleAPIKeyOnceRequest,
		IsMustProvideOneTimeKey: false,
	},
	APIServiceV1SecurityManagerGetServiceByAPIKey: {
		Path:                    []string{"api", "v1", "securitymanager", "api"},
		Method:                  "GET",
		Description:             "Get public key details if valid. If type is OneTime use, key is removed after this call",
		Callback:                HandleServiceByAPIKey,
		IsMustProvideOneTimeKey: false,
	},
}

// api key request handle
func HandleAPIKeyRequest(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// extract post data
	if err := request.ParseForm(); err != nil {
		fmt.Println("HandleAPIKeyRequest( )::Invalid data")
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}

	// get service name
	var serviceName string
	if values := request.Form["serviceName"]; values == nil || len(values) != 1 {
		fmt.Println("HandleAPIKeyRequest( )::can't get service name")
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	} else {
		serviceName = values[0]
	}

	// get service
	service := handle.(*Service)

	// extract authentication data
	if rawFullUserAuthentication := request.Header.Get("Authorization"); rawFullUserAuthentication != "" {
		if rawUserAuthentication := strings.Split(rawFullUserAuthentication,
			"Basic "); len(rawUserAuthentication) == 2 {
			if userAuthentication, err := base64.StdEncoding.DecodeString(rawUserAuthentication[1]); err == nil {
				if splitUserAuthentication := strings.Split(string(userAuthentication),
					":"); len(splitUserAuthentication) == 2 {
					ip, _, _ := net.SplitHostPort(request.RemoteAddr)
					if key, err := service.authenticate(splitUserAuthentication[0],
						splitUserAuthentication[1],
						serviceName,
						ip); err == nil {
						_, _ = rw.Write([]byte(key.ExportPrivate()))
						fmt.Println("delivered a 15mn token for",
							key.ServiceName)
						return http.StatusOK
					} else {
						fmt.Println("can't find login \"" + splitUserAuthentication[0] + "\":\""+ splitUserAuthentication[1]+"\":",err)
					}
				} else {
					fmt.Println("can't split \"" + string(userAuthentication) + "\"")
				}
			} else {
				fmt.Println("can't decode base64 Authorization header")
			}
		} else {
			fmt.Println("bad Basic Authorization header")
		}
	} else {
		fmt.Println("can't find Authorization header")
	}

	// bad request
	rw.WriteHeader(http.StatusBadRequest)
	return http.StatusBadRequest
}

// handle api key once valid request
func HandleAPIKeyOnceRequest(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// parse form
	if err := request.ParseForm(); err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}

	// get public key
	var publicKey, privateKey string
	if rawPublicKey := request.Form["publicKey"]; rawPublicKey == nil || len(rawPublicKey) != 1 {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	} else {
		publicKey = rawPublicKey[0]
	}
	if rawPrivateKey := request.Form["privateKey"]; rawPrivateKey == nil || len(rawPrivateKey) != 1 {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	} else {
		privateKey = rawPrivateKey[0]
	}

	// get service
	service := handle.(*Service)

	// try to request one time key
	if oneTimeKey, err := service.requestOneTimeKey(publicKey,
		privateKey); err == nil {
		_, _ = rw.Write([]byte(oneTimeKey.ExportPublic()))
		fmt.Println("delivered a one time token for",
			oneTimeKey.ServiceName)
		return http.StatusOK
	} else {
		rw.WriteHeader(http.StatusBadRequest)
		_, _ = rw.Write([]byte("{\"error\":\"" +
			err.Error() +
			"\"}"))
		return http.StatusBadRequest
	}
}

// handle public api key description request
func HandleServiceByAPIKey(rw http.ResponseWriter,
	request *http.Request,
	_ *common.Service,
	handle interface{}) int {
	// parse forms
	if err := request.ParseForm(); err != nil {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	}

	// get public key
	var publicKey string
	if rawPublicKey := request.Form["publicKey"]; rawPublicKey == nil || len(rawPublicKey) != 1 {
		rw.WriteHeader(http.StatusBadRequest)
		return http.StatusBadRequest
	} else {
		publicKey = rawPublicKey[0]
	}

	// get service
	service := handle.(*Service)

	// find key
	if key := service.findKey(publicKey); key != nil {
		_, _ = rw.Write([]byte(key.ExportPublic()))
		return http.StatusOK
	} else {
		rw.WriteHeader(http.StatusNotFound)
		return http.StatusNotFound
	}
}
